A simple python library for the [Matrix protocol](https://matrix.org).

Requires python 3.7+

Available on [PyPi](https://pypi.org/project/simplematrixlib/): `pip3 install simplematrixlib`.

Beware that I'm a beginner python user, any tips or pull requests are very
welcome.

https://gitlab.com/Swedneck/simplematrixlib
